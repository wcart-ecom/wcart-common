package com.wissen.wcart.common.constant;

public interface GlobalConstants {

	public static final String EMPTY = "";
	public static final String DOUBLE_COLON = "::";
	public static final String COLON = ":";
	public static final String SEMI_COLON = ";";
	public static final String HYPHEN = "-";
	public static final String UNDERSCORE = "_";
	public static final String SPACE = " ";

}
