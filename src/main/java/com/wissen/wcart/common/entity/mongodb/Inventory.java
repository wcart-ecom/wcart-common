package com.wissen.wcart.common.entity.mongodb;

import com.wissen.wcart.common.entity.mysql.Seller;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Inventory {
	
	private Seller seller;
	private Product product;
	private Long quantity;

}
