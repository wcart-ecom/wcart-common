package com.wissen.wcart.common.entity;

public enum ProductCategory {
	
	ELECTRONICS, FASHION, FOOD;

}
