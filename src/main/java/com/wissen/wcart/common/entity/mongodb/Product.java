package com.wissen.wcart.common.entity.mongodb;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.wissen.wcart.common.entity.ProductCategory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "products")
public class Product {
	
	@Id
	private Long productId;
	private String name;
	private String description;
	private Double price;
	private ProductCategory category;

}
