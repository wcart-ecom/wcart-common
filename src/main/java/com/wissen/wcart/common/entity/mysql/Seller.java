package com.wissen.wcart.common.entity.mysql;

import com.wissen.wcart.common.entity.SellerRating;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Seller {

	private Long sellerId;
	private String name;
	private String email;
	private String phone;
	private String address;
	private SellerRating rating;
	
}
