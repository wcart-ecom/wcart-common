package com.wissen.wcart.common.entity;

public enum Status {
	
	ACTIVE, INACTIVE, DELETED;

}
