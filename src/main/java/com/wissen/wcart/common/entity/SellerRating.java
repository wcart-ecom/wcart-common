package com.wissen.wcart.common.entity;

public enum SellerRating {
	
	BRONZE, SILVER, GOLD, PLATINUM;

}
