package com.wissen.wcart.common.dto;

import java.util.List;

import com.wissen.wcart.common.exception.Error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse {

	private List<Error> errors;
	
}
