package com.wissen.wcart.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WCartCommonApplication {

	public static void main(String[] args) {
		SpringApplication.run(WCartCommonApplication.class, args);
	}

}
