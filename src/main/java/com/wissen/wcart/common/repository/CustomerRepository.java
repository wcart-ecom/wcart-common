package com.wissen.wcart.common.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.wissen.wcart.common.entity.mysql.Customer;


@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

	/*
	 * @Query("SELECT cus FROM Customer cus WHERE cus.username = :username AND cus.password = password"
	 * ) public List<Customer> findByUserNamePassword(@Param("username") String
	 * username, @Param("password") String password);
	 */

}
