package com.wissen.wcart.common.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.wissen.wcart.common.entity.mongodb.Product;

public interface ProductRepository extends MongoRepository<Product, Long> {

}
