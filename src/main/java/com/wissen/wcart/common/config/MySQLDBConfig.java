package com.wissen.wcart.common.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories (basePackages = "com.wissen.wcart")
public class MySQLDBConfig {

}
