package com.wissen.wcart.common.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.wissen.wcart.common.dto.ErrorResponse;
import com.wissen.wcart.common.util.ErrorUtil;

@RestControllerAdvice
public class ExceptionHandler {

	public static final String INTERNAL_SERVER_ERROR_CODE = "WC_ERR_500";
	public static final String INTERNAL_SERVER_ERROR_KEY = "INTERNAL_SERVER_ERROR";

	public static final String BAD_REQUEST_CODE = "WC_ERR_400";
	public static final String BAD_REQUEST_KEY = "BAD_REQUEST";

	@org.springframework.web.bind.annotation.ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse captureAllException(Exception e) {
		return ErrorUtil.createErrorResponse(INTERNAL_SERVER_ERROR_CODE, INTERNAL_SERVER_ERROR_KEY,
				e.getLocalizedMessage());
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(NullPointerException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public ErrorResponse captureNullPointerException(NullPointerException e) {
		return ErrorUtil.createErrorResponse(INTERNAL_SERVER_ERROR_CODE, INTERNAL_SERVER_ERROR_KEY,
				e.getLocalizedMessage());
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse sendErrorResponse(MethodArgumentNotValidException e) {
		return ErrorUtil.formatException(BAD_REQUEST_CODE, BAD_REQUEST_KEY, e);
	}

	@org.springframework.web.bind.annotation.ExceptionHandler(MethodArgumentTypeMismatchException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ErrorResponse sendErrorResponse(
			org.springframework.web.method.annotation.MethodArgumentTypeMismatchException e) {
		return ErrorUtil.createErrorResponse(BAD_REQUEST_CODE, BAD_REQUEST_KEY, "Invalid Argument - " + e.getName());
	}

}
