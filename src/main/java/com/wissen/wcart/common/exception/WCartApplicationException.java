package com.wissen.wcart.common.exception;

public class WCartApplicationException extends RuntimeException {

	private static final long serialVersionUID = 3900551986652632851L;

	public WCartApplicationException() {
		super();
	}

	public WCartApplicationException(String message, Throwable cause) {
		super(message, cause);
	}

	public WCartApplicationException(String message) {
		super(message);
	}

	public WCartApplicationException(Throwable cause) {
		super(cause);
	}

}
