package com.wissen.wcart.common.util;

import static com.wissen.wcart.common.constant.GlobalConstants.*;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class LogUtil {

	public static void info(String serviceName, String className, String methodName, String message) {
		log.info(serviceName + SPACE + DOUBLE_COLON + SPACE + className + SPACE + DOUBLE_COLON + SPACE + methodName
				+ SPACE + DOUBLE_COLON + SPACE + message);
	}

}
