package com.wissen.wcart.common.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.wissen.wcart.common.dto.ErrorResponse;
import com.wissen.wcart.common.exception.Error;

public class ErrorUtil {

	public static ErrorResponse createErrorResponse(String code, String key, String message) {
		List<Error> errors = new ArrayList<>();
		errors.add(new Error(code, key, message));
		return new ErrorResponse(errors);
	}

	public static ErrorResponse formatException(String code, String key, MethodArgumentNotValidException e) {
		List<Error> errors = new ArrayList<>();
		for (FieldError fieldError : e.getBindingResult().getFieldErrors()) {
			errors.add(new Error(code, key, fieldError.getDefaultMessage()));
		}

		List<ObjectError> objectErrors = e.getBindingResult().getGlobalErrors();
		for (ObjectError error : objectErrors) {
			Error err = createGlobalValidationError(code, key, error);
			errors.add(err);
		}
		return new ErrorResponse(errors);
	}

	private static Error createGlobalValidationError(String code, String key, ObjectError error) {
		String errorKey = error.getDefaultMessage();
		return new Error(code, key, errorKey);
	}

}
